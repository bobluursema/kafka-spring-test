package com.example.accountservice;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.example.accountservice.producer.Producer;
import com.example.accountservice.repository.AccountRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class AccountService {
	@Autowired
	private Producer producer;
	@Autowired
	private AccountRepository repository;

	public void createAccount(Account account) throws JsonProcessingException {
		account.setCustomerVerified(false);
		Long accountId = repository.save(account).getId();
		Event event = new Event();
		event.setEventCreator("AccountService");
		event.setEventName("account_created");
		event.setEventTimeStamp(LocalDateTime.now());
		event.setId(UUID.randomUUID());

		ObjectMapper mapper = new ObjectMapper();

		String json = mapper.writeValueAsString(event);

		System.out.println("Sending message");

		producer.getMysource().output()
				.send(MessageBuilder.withPayload(json).setHeader("account_id", accountId).build());
	}

	public void updateAccount(String account_id) {
		Optional<Account> accountOpt = repository.findById(Long.parseLong(account_id));

		accountOpt.ifPresent(account -> {
			account.setCustomerVerified(true);
			repository.save(account);
		});
	}

}
