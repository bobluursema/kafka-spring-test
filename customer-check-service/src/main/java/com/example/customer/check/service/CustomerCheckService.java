package com.example.customer.check.service;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.example.customer.check.service.producer.Producer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CustomerCheckService {

	@Autowired
	private Producer producer;

	public void handleCustomerCreatedEvent(String accountId) throws JsonProcessingException {
		log.info(accountId);

		Event event = new Event();

		event.setEventCreator("CustomerCheckService");
		event.setEventName("CustomerHandled");
		event.setEventTimeStamp(LocalDateTime.now());
		event.setId(UUID.randomUUID());

		ObjectMapper mapper = new ObjectMapper();

		String json = mapper.writeValueAsString(event);

		producer.getMysource().output()
				.send(MessageBuilder.withPayload(json).setHeader("account_id", accountId).build());
	}
}
